#include "symtab.h"

static int indent = 0;
#define INDENT indent+=2
#define UNINDENT indent-=2
static int initial = 0;

static int hash(char *key) {
    int temp = 0;
    int i = 0;
    while (key[i] != '\0') {
        temp = ((temp << SHIFT) + key[i]) % HASHSIZE;
        i++;
    }
    return temp;
}

static void printBucketList(BucketList l) {
    int i;
    BucketList bl = l;
    while (bl != NULL) {
        for (i = 0; i < indent; i++)
            fprintf(listing, " ");
        fprintf(listing, "%-14s  %-8d  ", bl->name, bl->memloc);
        LineList t = bl->lines;
        while (t != NULL) {
            fprintf(listing, "%4d ", t->lineno);
            t = t->next;
        }
        fprintf(listing, "\n");
        bl = bl->next;
    }
}

static void printHashList(HashList hl) {
    int i;
    while (hl != NULL) {
        fprintf(listing, "Function Name:%-14s\n", hl->declName);
        for (i = 0; i < HASHSIZE; i++) {
            printBucketList(hl->bucketList[i]);
        }
        INDENT;
        printHashList(hl->child);
        UNINDENT;
        hl = hl->next;
    }
}

static void st_create(HashList parent, char *name, int memBase) {
    printf("in st_create %s\n", name);
    HashList hl = (HashList) malloc(sizeof(struct HashListRec));
    hl->parent = parent;
    /*when it has a parent and the parent is not the virtual global hashlist, set its child to the newly create hashlist*/
    if (parent != NULL && (strcmp(parent->declName, GLOBAL_HASHLIST_NAME) != 0))
    {
        parent->child = hl;
    }
    hl->declName = name;
    hl->child = NULL;
    hl->paranum = 0;
    hl->memBase = memBase;
    memset(hl->bucketList, 0, HASHSIZE);
    hl->next = NULL;
    hl->offset = 0;
    currentSymTab = hl;
    printf("out st_create %s\n", name);
}

static void st_attatch() {
    char *name = currentSymTab->declName;
    int key = hash(name);
    HashList hl = topHashTable[key];
    if (hl != NULL && strcmp(hl->declName, name) != 0)
        hl = hl->next;
    if (hl == NULL) {
        currentSymTab->next = topHashTable[key];
        topHashTable[key] = currentSymTab;
    }
}

/*static void st_initial(char* s)
{
//	printf("in st_intitial\n");
	//generate a virtual global table to store all the global variables and function variables
	st_create(NULL, s, 0);
	st_attatch();
//	printf("out st_initial\n");
}*/

MemLocation st_local_lookup(HashList hl, char *name) {
    MemLocation ml;
    int h = hash(name);
    if (hl == NULL) {
        ml.offset = -1;
        ml.type = EXPTYPE_UNKNOW;
    }
    else {
        BucketList bl = hl->bucketList[h];
        while (bl != NULL) {
            if (strcmp(bl->name, name) == 0)
                break;
            else
                bl = bl->next;
        }
        if (bl == NULL)/*can't find at this level*/
        {
            ml.offset = -1;
            ml.type = EXPTYPE_UNKNOW;
        }
        else/*find at this level*/
        {
            ml.offset = bl->memloc;
            ml.type = bl->type;
            ml.paramCnt = hl->paranum;
        }
    }
    ml.isfathervar = 0;
    return ml;
}

MemLocation st_lookup(HashList hl, char *name) {
    MemLocation ml = st_local_lookup(hl, name);
    if (ml.offset == -1) {
        if (hl->parent != NULL)
            ml = st_lookup(hl->parent, name);
        ml.isfathervar = 1;
    }
    return ml;
}

MemLocation st_insert(TreeNode *node, int lineno, int ispara, int isfuncname) {
    if (ispara == 1) {
        currentSymTab->paranum++;
    }
    int memloc = currentSymTab->memBase + currentSymTab->offset;
    int h = hash(node->attr.name);
    MemLocation ml;

    BucketList bl = currentSymTab->bucketList[h];
    while ((bl != NULL) && strcmp(bl->name, node->attr.name))
        bl = bl->next;
    if (bl == NULL)/*means this char is not in the table*/
    {
        bl = (BucketList) malloc(sizeof(struct BucketListRec));
        bl->name = node->attr.name;
        bl->lines = (LineList) malloc(sizeof(struct LineListRec));
        bl->lines->lineno = lineno;
        bl->lines->next = NULL;
        if (isfuncname == 1) {
            bl->type = EXPTYPE_FUNC;
        }
        else {
            bl->type = node->type;
        }
        bl->memloc = memloc;
        bl->next = currentSymTab->bucketList[h];
        currentSymTab->bucketList[h] = bl;/*circular*/
    }
    else/*it's in the table, refresh the lineno*/
    {
//		printf("st_insert: next-> insert new ll\n");
        LineList ll;
        ll = bl->lines;
        while ((ll = ll->next) != NULL)
            ll = ll->next;
        ll->next = (LineList) malloc(sizeof(struct LineListRec));
        ll = ll->next;
        ll->lineno = lineno;
        ll->next = NULL;
        ml.offset = bl->memloc;
        return ml;//no need to update bucketList offset
    }
    printf("%s->%d\n", currentSymTab->declName, currentSymTab->offset);
    printf("%s->%d\n", currentSymTab->declName, memloc);
    ml.offset = memloc;
    switch (node->type) {
        case EXPTYPE_CHAR:
        case EXPTYPE_INT:
        case EXPTYPE_REAL:
        case EXPTYPE_BOOL:
        case EXPTYPE_POINTER:
            currentSymTab->offset += 4;
            break;
        case EXPTYPE_ARRAY:
            currentSymTab->offset += (node->child[0]->attr.val) * 4;
            break;
        case EXPTYPE_VOID:
        default:
            break;
    }

    return ml;
}

void st_leave(TreeNode *node) {
    if (currentSymTab->parent != NULL) {

        printHashList(currentSymTab);
        currentSymTab = currentSymTab->parent;

    }
}

void st_insertdecl_var_list(TreeNode *node) {
    st_execute(node->child[1], node->child[1]->lineno);//计算pnode的类型
    node->child[0]->type = node->child[1]->type;
    printf("in st_insertdecl_var_list: nodetype=%d", node->child[1]->type);
    //把多个变量插入符号表
    node = node->child[0];
    while (1) {
        if (st_local_lookup(currentSymTab, node->attr.name).offset != -1)
            fprintf(listing, "Redefine identifier %s in line %d\n", node->attr.name, node->lineno);
        printf("%s\n", node->attr.name);
        st_insert(node, lineno, 0, 0);
        if (node->sibling != NULL) {
            node->sibling->type = node->type;
            node = node->sibling;
        }
        else {
            break;
        }
    }
    return;
}

MemLocation st_execute(TreeNode *node, int lineno) {
    MemLocation ml;
    switch (node->nodekind) {
        case NODE_EXPRESSION: //是表达式节点 要返回表达式(变量和函数的)的位置信息，并且检查改变量是否定义
        {
            switch (node->kind.exp) {
                case EXP_FUNC:    /*return the call function addr*/
                    ml = st_lookup(currentSymTab, node->attr.name);
                    if (ml.offset == -1)
                        fprintf(listing, "Undefine identifier %s in line %d\n", node->attr.name, lineno);
                    return ml;
                    break;
                case EXP_ID:
                    ml = st_lookup(currentSymTab, node->attr.name);
                    if (ml.offset == -1)
                        fprintf(listing, "Undefine identifier %s in line %d\n", node->attr.name, lineno);
                    return ml;
                    break;
                default:
                    break;
            }
        }
            break;
        case NODE_DECLARE://是定义的节点，所以要插入符号表，检查函数是否重定义
        {
            switch (node->kind.decl) {
                case DECL_ROUTINEHEAD: //only create the st when the first time
                    if (initial == 0) {
                        st_create(currentSymTab, node->attr.name, 0);
                        st_attatch();
                        initial = 1;
                    }
                    break;
                case DECL_FUNCTION:
                    printf("in DECL_FUNCTION\n");
                    node->type = node->child[0]->type;
                    if (st_local_lookup(currentSymTab, node->attr.name).offset != -1)
                        fprintf(listing, "Redefine identifier %s in line %d\n", node->attr.name, lineno);
                    st_insert(node, lineno, 0, 1);

                    st_create(currentSymTab, node->attr.name, 0);
                    st_attatch();
                    break;
                case DECL_PROCEDURE:
                    printf("in DECL_PROCEDURE\n");
                    printf("nodetype=%d\n", node->type);
                    node->type = EXPTYPE_VOID;
                    if (st_local_lookup(currentSymTab, node->attr.name).offset != -1)
                        fprintf(listing, "Redefine identifier %s in line %d\n", node->attr.name, lineno);
                    st_insert(node, lineno, 0, 1);
                    st_create(currentSymTab, node->attr.name, 0);
                    st_attatch();
                    break;
                case DECL_CONST:
                    //**********
                    break;
                case DECL_TYPE:
                    //**********
                    break;
                case DECL_VAL_PARA:        //所有变量用相同的方法处理
                case DECL_VAR_PARA:
                    st_execute(node->child[1], node->child[1]->lineno);//计算pnode的类型
                    node->child[0]->type = node->child[1]->type;
                    //把多个变量插入符号表
                    node = node->child[0];
                    while (1) {
                        if (st_local_lookup(currentSymTab, node->attr.name).offset != -1)
                            fprintf(listing, "Redefine identifier %s in line %d\n", node->attr.name, node->lineno);
                        st_insert(node, lineno, 1, 0);
                        if (node->sibling != NULL) {
                            node->sibling->type = node->type;
                            node = node->sibling;
                        }
                        else { break; }
                    }
                    break;

                case DECL_VAR:
                    st_execute(node->child[1], node->child[1]->lineno);//计算pnode的类型
                    node->child[0]->type = node->child[1]->type;
                    //把多个变量插入符号表
                    node = node->child[0];
                    while (1) {
                        if (st_local_lookup(currentSymTab, node->attr.name).offset != -1)
                            fprintf(listing, "Redefine identifier %s in line %d\n", node->attr.name, node->lineno);
                        st_insert(node, lineno, 0, 0);
                        if (node->sibling != NULL) {
                            node->sibling->type = node->type;
                            node = node->sibling;
                        }
                        else { break; }
                    }
                    break;

                default :
                    fprintf(listing, "unknown declaration node.\n");
                    break;
            }
        }
            break;
        case NODE_TYPE: //类型节点 功能是计算类型
        {
            switch (node->kind.type) {
                case TYPE_SIMPLE_SYS:
                    printf("in TYPE_SIMPLE_SYS\n");
                    break;
                case TYPE_SIMPLE_ID:
                    ml = st_lookup(currentSymTab, node->attr.name);
                    if (ml.type == EXPTYPE_UNKNOW)
                        fprintf(listing, "Undefine identifier %s\n", node->attr.name);
                    node->type = ml.type;
                    break;
                case TYPE_SIMPLE_ENUM:
                    node->type = EXPTYPE_ENUM;
                    //处理枚举类型的定义
                    break;
                case TYPE_SIMPLE_LIMIT:
                    //处理limit类型的定义
                    node->type = EXPTYPE_LIMIT;
                case TYPE_ARRAY:
                    node->type = EXPTYPE_ARRAY;
                    //
                    break;
                case TYPE_RECORD:
                    node->type = EXPTYPE_RECORD;
                    //
                    break;
                default:
                    node->type = EXPTYPE_UNKNOW;
                    break;
            }
        }
        default:
            fprintf(listing, "unknown node kind.\n");
            break;
    }
    ml.isfathervar = 0;
    ml.offset = -1;
    return ml;
}


/*void st_makeSpaceForReturn()
{
	currentSymTab->offset += 8;
}*/

void traverse(TreeNode *t, MemLocation (*preProc)(TreeNode *, int),
              void (*postProc)(TreeNode *)) {
    if (t != NULL) {
//		printf("before preProc\n");
        preProc(t, t->lineno);
        {
            int i;
            for (i = 0; i < MAXCHILDREN; i++)
                traverse(t->child[i], preProc, postProc);
        }
//		printf("before postProc %d\n", (t==NULL));
        postProc(t);
//		printf("before searching sibling\n");
        traverse(t->sibling, preProc, postProc);
    }
//	printf("out tranverse\n");
}

void buildSymTab(TreeNode *node) {
    traverse(node, st_execute, st_leave);
}

void printSymTab() {
    int i;
    fprintf(listing, "Variable Name\tLocation\tLine Number\n");
    fprintf(listing, "--------------\t---------\t--------------\n");
    for (i = 0; i < HASHSIZE; i++) {
        printHashList(topHashTable[i]);
    }
}


