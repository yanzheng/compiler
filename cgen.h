#ifndef _CODEGEN_H
#define _CODEGEN_H

#include <stdio.h>
#include "global.h"
#include "symtab.h"
#include "y.tab.h"

int buildCode(TreeNode *pnode);

void initialGenerator(char *o_name);

int generateCode(TreeNode *pnode);

void GExpCallCode(TreeNode *pnode);

extern void GExpCode(TreeNode *pnode, int isrightvalue);

void GExpConstCode(TreeNode *pnode);

#endif
