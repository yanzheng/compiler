#include "cgen.h"
#include "util.h"

#define CG_OUTPUT(asm) fprintf(CG_CODEFILE, "%s", asm);

FILE *CG_CODEFILE = NULL;
static int err_cnt = 0;

static int varCnt = 0;
static int paramCnt = 0;

// 如果是函数变量的话要先执行函数
int getOffset(TreeNode *pnode) {
    int re = 0;
    int tmp = st_execute(pnode, pnode->lineno).offset;
    int tmparamCnt = st_execute(pnode, pnode->lineno).paramCnt;
    int cnt = tmp / 4;
    if (cnt < tmparamCnt)
        re = tmp + 8;
    else
        re = 0 + tmparamCnt * 4 - tmp - 4;
    return re;
}

int isFatherParameter(TreeNode *pnode) {
    int tmp1 = st_execute(pnode, pnode->lineno).isfathervar;
    return (tmp1 == 1);
}

/**
 * Generate Label Increase by 1;
 * Used in Jmp or Jmp with Condition
**/
char *GetLabel() {
    static int label_cnt = 0;
    char tmp2[1024];
    static char label[1024];
    strcpy(label, "__CG__label");
    sprintf(tmp2, "%d", label_cnt);
    strcat(label, tmp2);
    label_cnt++;
    return label;
}

void GStmtIfCode(TreeNode *pnode) {
    char if_label[100], else_label[100], exit_label[100], tmp[1024];
    strcpy(if_label, GetLabel());
    strcpy(else_label, GetLabel());
    strcpy(exit_label, GetLabel());

    generateCode(pnode->child[0]);
    sprintf(tmp, "cmp eax, 1\nje %s\t\t; If equal Jump to taken-content; \njmp %s\t\t; Jump to NotTake Content\n",
            if_label, else_label);
    CG_OUTPUT(tmp);

    sprintf(tmp, "%s: \n", if_label);
    CG_OUTPUT(tmp);
    generateCode(pnode->child[1]);
    memset(tmp, 0, 1024);
    sprintf(tmp, "jmp %s\t\t; Jump to The if exit Label\n", exit_label);
    CG_OUTPUT(tmp);

    sprintf(tmp, "%s: \n", else_label);
    CG_OUTPUT(tmp);
    if (pnode->child[2] != NULL) {
        generateCode(pnode->child[2]);
    }
    sprintf(tmp, "%s: \n", exit_label);
    CG_OUTPUT(tmp);
}

void GStmtForCode(TreeNode *pnode) {
    // TODO: fix var bug

    char for_start[100], for_end[100], tmp[1024];
    strcpy(for_start, GetLabel());
    strcpy(for_end, GetLabel());

    if (pnode->attr.op == TOKEN_TO) {
        generateCode(pnode->child[1]);
        CG_OUTPUT("mov ecx, eax \t\t;ecx = for1\n");
        generateCode(pnode->child[2]);
        CG_OUTPUT("mov edx, eax \t\t;edx = for2\n");
        CG_OUTPUT("inc edx\n")
        sprintf(tmp, "%s:\n cmp ecx, edx\n je %s\t\t;for end\n", for_start, for_end);
        CG_OUTPUT(tmp);
        CG_OUTPUT("push edx\t\t;save for2\n");
        CG_OUTPUT("push ecx\t\t;save for1\n");
        generateCode(pnode->child[3]);
        CG_OUTPUT("pop ecx\t\t;restore for1\n");
        CG_OUTPUT("pop edx\t\t;restore for2\n");
        sprintf(tmp, "inc ecx\n jmp %s\n %s:\n", for_start, for_end);
        CG_OUTPUT(tmp);
    }
    else {
        generateCode(pnode->child[1]);
        CG_OUTPUT("mov ecx, eax\t\t;ecx = for1\n");
        generateCode(pnode->child[2]);
        CG_OUTPUT("mov edx, eax\t\t;edx = for2\n");
        CG_OUTPUT("dec edx\n");

        sprintf(tmp, "%s:\n cmp ecx, edx\n je %s\t\t;for end\n", for_start, for_end);
        CG_OUTPUT(tmp);
        generateCode(pnode->child[3]);
        sprintf(tmp, "dec ecx\njmp %s\n %s:\n", for_start, for_end);
        CG_OUTPUT(tmp);
        sprintf(tmp, "%s:\n", for_end);
        CG_OUTPUT(tmp);
    }
}

void GStmtWhileCode(TreeNode *pnode) {
    char while_start[100], while_end[100], tmp[1024];
    strcpy(while_start, GetLabel());
    strcpy(while_end, GetLabel());
    sprintf(tmp, "%s:\t\t;while start\n", while_start);
    CG_OUTPUT(tmp);
    generateCode(pnode->child[0]);
    sprintf(tmp, "cmp eax, 0\n je %s\t\t;while break\n", while_end);
    CG_OUTPUT(tmp);
    generateCode(pnode->child[1]);
    sprintf(tmp, "jmp %s\n%s:\n", while_start, while_end);
    CG_OUTPUT(tmp);
}

void GStmtRepeatCode(TreeNode *pnode) {
    char repeat_start[100], repeat_end[100], tmp[1024];
    strcpy(repeat_start, GetLabel());
    strcpy(repeat_end, GetLabel());
    sprintf(tmp, "%s:\t\t;repeat start\n", repeat_start);
    CG_OUTPUT(tmp);
    generateCode(pnode->child[0]);
    generateCode(pnode->child[1]);
    sprintf(tmp, "cmp eax, 0\n je %s\t\t;repeat continue\n", repeat_start);
    CG_OUTPUT(tmp);
    sprintf(tmp, "%s:\n", repeat_end);
    CG_OUTPUT(tmp);
}

//void GStmtCompCode(TreeNode *pnode) {
//    // TODO: for what
//    generateCode(pnode->child[0]);
//    generateCode(pnode->child[1]);
//    return;
//}

/**
 * Generate Code for Return Instr.
 * Assign the return value to EAX
**/
//void GStmtRetCode(TreeNode *pnode) {
//    generateCode(pnode->child[0]);
//    for (int i = 0; i < varCnt; i++)
//        CG_OUTPUT("pop ebx\n");
//    varCnt = 0;
//    CG_OUTPUT(POP_ALL);
//    CG_OUTPUT("ret\n");
//}

void GStmtInputCode(TreeNode *pnode) {
    // TODO
    char tmp[200];
    char code[200];
#ifdef CG_DEBUG
		printf("\tInput. \n");
#endif
    CG_OUTPUT("call input\n");
    //generateCode(pnode->child[0]);
    sprintf(code, "mov edi, %d\n",
            getOffset(pnode->child[0]));//st_execute(pnode->child[0], pnode->child[0]->lineno).offset);
    if (isFatherParameter(pnode->child[0])) {
        sprintf(tmp, "mov ss:[esi+edi], eax\n");
    }
    else {
        sprintf(tmp, "mov ss:[ebp+edi], eax\n");

    }
    strcat(code, tmp);
    CG_OUTPUT(code);
}

void GStmtOutputCode(TreeNode *pnode) {
    // TODO: test real
//    if (pnode->child[1]->kind.exp == EXP_ID) {
//        CG_OUTPUT("invoke  crt_printf, ADDR PrintInteger, eax\n");
//    }
//    else if (pnode->child[1]->kind.exp == EXP_CONST) {
//        switch (pnode->child[1]->type) {
//            case EXPTYPE_INT:
//                GExpConstCode(pnode->child[1]);
//                CG_OUTPUT("invoke  crt_printf, ADDR PrintInteger, eax\n");
//                break;
//            case EXPTYPE_REAL:
//                GExpConstCode(pnode->child[1]);
//                CG_OUTPUT("invoke  crt_printf, ADDR PrintReal, eax\n");
//                break;
//        }
//    }

    pnode->attr.name = copyString("output");

    GExpCallCode(pnode);
}

void GStmtNewLine() {
    CG_OUTPUT("invoke  crt_printf, ADDR NewLine\n");
}

void GStmtCode(TreeNode *pnode) {
    /* TODO */
    switch (pnode->kind.stmt) {
        case STMT_IF:
            GStmtIfCode(pnode);
            break;
        case STMT_WHILE:
            GStmtWhileCode(pnode);
            break;
        case STMT_REPEAT:
            GStmtRepeatCode(pnode);
            break;
        case STMT_FOR:
            GStmtForCode(pnode);
            break;
        case STMT_PROC_SYS: {
            switch (pnode->attr.op) {
                case TOKEN_READ:
                    GStmtInputCode(pnode);
                    break;
                case TOKEN_WRITE:
                    // TODO: remove new line
                    GStmtOutputCode(pnode);
                    break;
                case TOKEN_WRITELN:
                    // TODO: fix new line
                    GStmtOutputCode(pnode);
                    // GStmtNewLine(pnode);
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    CG_OUTPUT("\n");
    return;
}

/**
 * Generate Code for pushing parameters for call function
 * We push the param from right to left.
**/
int pushParam(TreeNode *pnode) {
    int cnt = 1;
    /* TODO */
    if (pnode->sibling != NULL)
        cnt += pushParam(pnode->sibling);
    CG_OUTPUT(";function parameter start\n");
    printf("pushparam\n");
    GExpCode(pnode, 0);
    CG_OUTPUT("push eax\n");
    CG_OUTPUT(";function parameter end\n");
    return cnt;
}

/**
 * Generate Code for Calling a Function
 * Pass the parameters by stack.
 * Push from right to left.
**/
void GExpCallCode(TreeNode *pnode) {
    char tmp[1024];
    int cnt = 0, i;
    /* TODO */
    if (pnode->child[1] != NULL) {
        TreeNode *tmp = pnode->child[1];
        {
            /* use post order*/
            printf("Here:::%s\n", pnode->attr.name);
            cnt = pushParam(tmp);
        }
    }
    memset(tmp, 0, 1024);
    sprintf(tmp, "call %s\n", pnode->attr.name);
    CG_OUTPUT(tmp);
    for (i = 0; i < cnt; i++) CG_OUTPUT("pop ebx\t\t;pop parameter in order to banlance the stack\n");

    /*return the function value
    sprintf(tmp,"mov edi, %d\n", getOffset(pnode));
    if(isFatherParameter(pnode)){
        strcat(tmp, "mov eax, ss:[esi+edi] \n");
    }
    else{
        strcat(tmp, "mov eax, ss:[ebp+edi] \n");//st_execute(pnode, pnode->lineno).offset);
    }
    CG_OUTPUT(tmp);*/

    return;
}

/**
 * Generate Code for a const number use in code.
 * We simple move it to eax register.
**/
void GExpConstCode(TreeNode *pnode) {
    char tmp[1024];
    switch (pnode->type) {
        case EXPTYPE_REAL:
            // TODO
            sprintf(tmp, "mov eax, %f\n", pnode->attr.real_val);
            break;
        case EXPTYPE_INT:
            sprintf(tmp, "mov eax, %d\n", pnode->attr.val);
            break;
    }
    CG_OUTPUT(tmp);
    return;
}

void GExpOpCode(TreeNode *pnode)
/**
 * Generate Code for Operation Calculating
 * Use eax as the final value.
 * Cal the data with eax and ebx.
**/
{
    char code[1024], tmp[1024];
    //CG_OUTPUT("xor eax, eax\nxor ebx, ebx\n");
    if (pnode->child[0] != NULL)
        GExpCode(pnode->child[0], 0);
    CG_OUTPUT("push eax\n");
    if (pnode->child[1] != NULL)
        GExpCode(pnode->child[1], 1);
    CG_OUTPUT("mov ebx, eax\n");
    CG_OUTPUT("pop eax\n");
    strcpy(code, "");


    //进行类型检查， 判断两个操作数的类型是否一致
    //
    //
    if ((pnode->child[0] != NULL && pnode->child[0]->type != EXPTYPE_REAL) &&
        (pnode->child[1] != NULL && pnode->child[1]->type != EXPTYPE_REAL)) {
        switch (pnode->attr.op) {
            case TOKEN_PLUS:
                strcat(code, "add eax, ebx\n");
                break;
            case TOKEN_MINUS:
                strcat(code, "sub eax, ebx\n");
                break;
            case TOKEN_MUL:
                strcat(code, "xor edx, edx\nmul ebx\n");
                break;
            case TOKEN_DIV:
                strcat(code, "xor edx, edx\ndiv ebx\n");
                break;
            case TOKEN_LT:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "setl al\n");
                break;
            case TOKEN_LE:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "setng al\n");
                break;
            case TOKEN_GT:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "setg al\n");
                break;
            case TOKEN_GE:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "setnl eax\n");
                break;
            case TOKEN_EQUAL:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "sete al\n");
                break;
            case TOKEN_UNEQUAL:
                strcat(code, "cmp eax, ebx\n");
                strcat(code, "mov eax, 0\n");
                strcat(code, "setne al\n");
                break;
            case TOKEN_ASSIGN:
                /* TODO */
                strcat(code, "mov eax, ebx\n");
                sprintf(tmp, "mov edi, %d\n",
                        getOffset(pnode->child[0]));//st_execute(pnode->child[0], pnode->child[0]->lineno).offset);
                strcat(code, tmp);
                if (isFatherParameter(pnode->child[0])) {
                    sprintf(tmp, "mov ss:[esi+edi], eax\n");
                }
                else {
                    sprintf(tmp, "mov ss:[ebp+edi], eax\n");
                }
                strcat(code, tmp);
                break;
            default:
                printf("error %d(%d): Unknown Opeartion Type, Plase Update your CodeGen\n", ++err_cnt, pnode->lineno);
        }
    }
    else
        printf("error %d(%d): Do not support float calculate\n", ++err_cnt, pnode->lineno);
    CG_OUTPUT(code);
    return;
}

/**
 * Generate the code for Expressiong Instruction.
 * Concern on type: Call Function, Op Operation, Const Operation, EXP_ID Operation.
**/
void GExpCode(TreeNode *pnode, int isrightvalue) {
    char tmp[1024];

    switch (pnode->kind.exp) {
        case EXP_FUNC:
            CG_OUTPUT(";call function\n");
            GExpCallCode(pnode);
            break;
        case EXP_OP:
            GExpOpCode(pnode);
            break;
        case EXP_CONST:
            GExpConstCode(pnode);
            break;
        case EXP_ID:
            // TODO: do
            //if the ID is a function ,first execute this function
            if ((st_execute(pnode, pnode->lineno).type == EXPTYPE_FUNC) && (pnode->child[1] == NULL) &&
                (isrightvalue == 1)) {  //是函数变量
                printf("%d\n", pnode->child[1] == NULL);
                sprintf(tmp, ";the ID is function\ncall %s\n", pnode->attr.name);
                CG_OUTPUT(tmp);
            }
            else {
                sprintf(tmp, "mov edi, %d\n", getOffset(pnode));
                if (isFatherParameter(pnode)) {
                    strcat(tmp, "mov eax, ss:[esi+edi] \n");
                }
                else {
                    strcat(tmp, "mov eax, ss:[ebp+edi] \n");//st_execute(pnode, pnode->lineno).offset);
                }
                CG_OUTPUT(tmp);
            }
            break;
    }
    return;
}

/**
 * For Deceleration Type Instruction.
**/
void GDeclVarCode(TreeNode *pnode) {
    // TODO: array
    if (pnode->attr.op == TOKEN_ARRAY) {

    } else {
        varCnt++;
        printf("\nvarCnt=%d\n", varCnt);
        CG_OUTPUT("push eax\t\t; Allocate the space in stack to a Varable\n");
    }
}

void GDeclFuncCode(TreeNode *pnode) {
    // TODO: check
    char tmp[1024];

    generateCode(pnode->child[0]); //calculate the type of function
    generateCode(pnode->child[1]); //push the parameter of function

    pnode->child[2]->attr.name = copyString(pnode->attr.name);
    generateCode(pnode->child[2]); // generate the code of function body


    sprintf(tmp, "add esp, %d\n", (varCnt) * 4);
    CG_OUTPUT(tmp);
    varCnt = 0;

    CG_OUTPUT("pop ebp\n");
    CG_OUTPUT("ret\n");
    st_leave(pnode->child[3]);
    paramCnt = 0;
}

void GParamCode(TreeNode *pnode) {
    paramCnt++;
    printf("\nparamCnt=%d\n", paramCnt);
    return;
}

void GDeclCode(TreeNode *pnode) {
    int i;
    char tmp[200];
    switch (pnode->kind.decl) {
        case DECL_ROUTINEHEAD:
            printf("DECL_ROUTINEHEAD: %s\n", pnode->attr.name);
            if (pnode->child[2] != NULL) {
                st_insertdecl_var_list(pnode->child[2]);//only insert the var into symtable
            }
            if (pnode->child[3] != NULL) {
                generateCode(pnode->child[3]); //generate the function(procedure) code

            }
            if (pnode->attr.name != NULL) {
                sprintf(tmp, "%s:\n", pnode->attr.name);
                CG_OUTPUT(tmp);
                CG_OUTPUT("push ebp\nmov ebp, esp\nmov esi, ss:[ebp+0]\n");
            }
            for (i = 0; i < 2; i++) {
                if (pnode->child[i] != NULL) {
                    generateCode(pnode->child[i]);
                }
            }
            if (pnode->child[2] != NULL) {
                GDeclCode(pnode->child[2]); // use this function can void insert the var two times
            }

            break;
        case DECL_FUNCTION:
            return GDeclFuncCode(pnode);
        case DECL_PROCEDURE:
            return GDeclFuncCode(pnode);
        case DECL_CONST:
            //******************
            break;
        case DECL_TYPE:
            //******************
            break;
        case DECL_VAR:
            pnode = pnode->child[0];
            while (pnode != NULL) {
                GDeclVarCode(pnode);
                pnode = pnode->sibling;
            }
            break;
        case DECL_VAL_PARA:
        case DECL_VAR_PARA:
            GParamCode(pnode);
            break;
        default:
            printf("Error %d(%d): Unkown Decl Type. Please Check\n", ++err_cnt, pnode->lineno);
            break;
    }
    return;
}

/**
 * Initial Operations For Code Generater.
**/
void initialGenerator(char *o_name) {
    CG_CODEFILE = fopen(o_name, "w");
}

/**
 * Generate Assemble Code
 * Use Post Order Recursive to Generate Code
 * Use Prior Oder Recursive to Generate
**/
int generateCode(TreeNode *pnode) {
    if (pnode == NULL) {
        return err_cnt;
    }
    st_execute(pnode, pnode->lineno);
    switch (pnode->nodekind) {
        case NODE_STATEMENT:
            GStmtCode(pnode);
            break;
        case NODE_EXPRESSION:
            GExpCode(pnode, 0);
            break;
        case NODE_DECLARE:
            GDeclCode(pnode);
            break;
        default:
            break;
    }
    CG_OUTPUT("\n");
    //st_leave(pnode);
    if (pnode->sibling != NULL) {
        pnode->sibling->type = pnode->type;
        generateCode(pnode->sibling);
    }
    return err_cnt;
}

void generateCodeHead(char *s) {
    CG_OUTPUT(".386\n");
    CG_OUTPUT(".model flat,stdcall\n");
    CG_OUTPUT("option casemap:none\n");
    CG_OUTPUT("\n");
    CG_OUTPUT("include    masm32\\include\\windows.inc\n");
    CG_OUTPUT("include    masm32\\include\\kernel32.inc\n");
    CG_OUTPUT("include    masm32\\include\\msvcrt.inc\n");
    CG_OUTPUT("includelib masm32\\lib\\msvcrt.lib\n");
    CG_OUTPUT("includelib masm32\\lib\\kernel32.lib\n");
    CG_OUTPUT("include    masm32\\include\\user32.inc\n");
    CG_OUTPUT("includelib masm32\\lib\\user32.lib\n");
    CG_OUTPUT("includelib masm32\\lib\\kernel32.lib\n");
    CG_OUTPUT("includelib masm32\\lib\\msvcrt.lib\n");
    CG_OUTPUT("printf PROTO C :ptr sbyte, :vararg\n");

    CG_OUTPUT("	.data\n");
    CG_OUTPUT("szInput        db  '%d', 0\n");
    CG_OUTPUT("szFormat    db    '%d',0ah, 0dh , 0\n");
    CG_OUTPUT("tmp			db 0, 0, 0, 0, 0, 0, 0, 0\n");
    CG_OUTPUT("    .const\n");

    CG_OUTPUT("    .code \n");
    CG_OUTPUT("start:  \n");
    CG_OUTPUT("mov esi, ebp\n");
    CG_OUTPUT("	push ebp\n");
    CG_OUTPUT("	mov ebp, esp\n");
    CG_OUTPUT("	call ");
    CG_OUTPUT(s);

    CG_OUTPUT("\npop ebp\n");
    CG_OUTPUT("\n");
    CG_OUTPUT("    invoke    ExitProcess,NULL\n");
    CG_OUTPUT("\n");
    CG_OUTPUT("output:\n");
    CG_OUTPUT("	push    ebp\n");
    CG_OUTPUT("    mov     ebp, esp\n");
    CG_OUTPUT("    push    eax\n");
    CG_OUTPUT("    mov     ebx, offset szFormat\n");
    CG_OUTPUT("    push    ebx\n");
    CG_OUTPUT("    call    printf\n");
    CG_OUTPUT("\n");
    CG_OUTPUT("	pop		ebx\n");
    CG_OUTPUT("	pop		ebx\n");
    CG_OUTPUT("\n");
    CG_OUTPUT("    pop     ebp\n");
    CG_OUTPUT("    ret\n");
    CG_OUTPUT("	ret\n");
    CG_OUTPUT("input:\n");
    CG_OUTPUT("	invoke crt_scanf, addr szInput, addr tmp\n");
    CG_OUTPUT("	mov eax, dword ptr tmp\n");
    CG_OUTPUT("	ret\n");
}

void generateCodeTail() {
    char tmp[200];
    printf("varCnt = %d\n", varCnt);
    sprintf(tmp, "add esp, %d\n", (varCnt) * 4);
    CG_OUTPUT(tmp);
    CG_OUTPUT("pop ebp\n");
    CG_OUTPUT("ret\n");
    CG_OUTPUT("end    start\n");
}

int buildCode(TreeNode *pnode) {
    printf("Program: %s\n", pnode->attr.name);

    generateCodeHead(pnode->attr.name);
    generateCode(pnode);
    generateCodeTail();

    printf("%s\n", currentSymTab->declName);
    return 0;
}
