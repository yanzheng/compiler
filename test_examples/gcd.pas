program hello;
var 
    ans : integer;

function func(a : integer) : integer;
begin
    if a = 1 then begin
        func := 1;
    end
    else begin
        if a = 2 then begin
            func := 1;
        end
        else begin
            func := func(a - 2);
            func := func + func(a - 1);
        end;
    end;
end;

begin
    ans := func(5);
    writeln(ans);
end.
