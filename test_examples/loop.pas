program right;
var a: integer;
begin

a := 1;

repeat
begin
    writeln(a);
    a := a + 1;
end;
until a = 3;

for a := 1 to 3 do
begin
    writeln(99);
end;

while a < 6 do
begin
  writeln (a);
  a := a + 1;
end;

end.
