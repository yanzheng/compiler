program bas;
var x, y, z : integer;
begin
  x := 2;
  writeln(x);
  y := 7;
  writeln(y);
  z := 9;
  writeln(z);
  x := y + z;
  writeln(x);
  x := y - z;
  writeln(x);
  x := y * z;
  writeln(x);
end.
