program hello;
var
    i : integer;

function go(a : integer): integer;
begin
    if a = 1 then
    begin
        go := 100;
    end
    else
    begin
        if a = 2 then
        begin
            go := 90;
        end
        else
        begin
            go := 80;
        end;
    end;
end;

begin
    i := go(1);
    write(i);
    i := go(2);
    write(i);
    i := go(3);
    write(i);
end.
