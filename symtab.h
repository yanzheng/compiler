#ifndef _SYMTAB_H_
#define _SYMTAB_H_

#include "global.h"

#define HASHSIZE 221
#define SHIFT 4

#define GLOBAL_HASHLIST_NAME "main program"
#define COMPOUND_HASHLIST_NAME "{}"

extern FILE *listing;
/********************Every variable owns a LineList to store its lineno*/
struct LineListRec {
    int lineno;
    struct LineListRec *next;
};
typedef struct LineListRec *LineList;

/*****************************with extra variable information, BucketList is designed to resolve hash confiction*/
typedef struct BucketListRec {
    char *name;
    ExpType type;
    int memloc;
    struct BucketListRec *next;
    LineList lines;

} *BucketList;

/********************Every function owns a HashList to store all its local variables. Global variabls are specail to some extend, thus we manually specify  a global HashList to store global variables and function return variables. It also resolves hash conflict.*/

typedef struct HashListRec/*one points to global, the rest stores all the function declaration*/
{
    char *declName;
    //global decalration should be '\0'
    struct HashListRec *child;
    struct HashListRec *parent;
    BucketList bucketList[HASHSIZE];
    struct HashListRec *next;
    int memBase; //该函数的基地址 每次进入一个新过程时需要重设这个变量
    int offset;
    /*used to record bucketList->memloc*/
    int paranum; //thr number of parameter (if this is a function)
} *HashList;

/*************************The top table combines global and function*/
HashList topHashTable[HASHSIZE];

HashList currentSymTab;

typedef struct {
    int paramCnt;
    int offset;
    //-1 if not exist
    ExpType type;
    int isfathervar; //0 :正常 1： 是FatherParameter
} MemLocation;

MemLocation st_execute(TreeNode *node, int lineno);

//the final function used
void st_insertdecl_var_list(TreeNode *node);

void st_leaveLocal(TreeNode *node);//call when go out {} node or function

void printSymTab();

void st_leave(TreeNode *node);

#endif
