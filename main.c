#include "global.h"
#include "util.h"
#include "parse.h"
#include "analyze.h"
#include "cgen.h"

char in_filename[FILENAME_MAX];
char out_filename[FILENAME_MAX];

FILE *source;
FILE *listing;
int TraceScan = True;
int lineno = 0;

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("usage: %s example.pas example.asm\n", argv[0]);
        return 1;
    }

    strcpy(in_filename, argv[1]);
    strcpy(out_filename, argv[2]);

    source = fopen(in_filename, "r");
    if (source == NULL) {
        printf("file does not exist.\n");
        return 1;
    }

    listing = stdout;

    TreeNode *syntaxTree;
    syntaxTree = parse();
    printTree(syntaxTree);

    initialGenerator(out_filename);

    printf("Begin Coding: \n");
    buildCode(syntaxTree);

    printf("The SymTab is :\n");
    printSymTab();

    return 0;
}
