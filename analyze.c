#include "analyze.h"

ExpType map[1000];

int hash(char *name) {
    int ret = 0;
    for (int i = 0; i < strlen(name); ++i) {
        ret += (i + 1) * ((int) name[i] + 3);
        ret %= 1000;
    }
    return ret;
}

void parseDecl(TreeNode *t) {
    while (t) {
        switch (t->nodekind) {
            char *name;
            case NODE_DECLARE:
                name = t->child[0]->attr.name;
                ExpType type = t->child[1]->type;
                map[hash(name)] = type;
        }
        t = t->sibling;
    }
}

void checkType(TreeNode *t) {
    while (t) {
        switch (t->attr.op) {
            char *name;
            case TOKEN_ASSIGN:
                name = t->child[0]->attr.name;
                ExpType exp1 = map[hash(name)];
                ExpType exp2 = t->child[1]->type;
                if (exp1 != exp2) {
                    printf("Type error: line %d\n", t->child[0]->lineno);
                }
                break;
        }
        t = t->sibling;
    }
}

void typeCheck(TreeNode *t) {
    if (t) {
        parseDecl(t);
        checkType(t);
    }
}
