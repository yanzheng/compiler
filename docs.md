# 编译系统设计实验文档

闫铮

3110000125

yzheng624@gmail.com

## 如何编译

	yacc -d yacc/pascal.y			# create y.tab.h, y.tab.c
	flex lex/pascal.l				# create lex.yy.c
	mkdir build						# create folder for build
	cmake ..						# generate Makefile using CMake
	make							# compile codes


## 整体构架

架构主要分为三个部分，也就是词法分析、语法分析和代码生成。

### 词法分析

第一部分是词法分析。也就是把输入的源代码中的字符串转化为 token。通过使用正则表达式，我们可以区分不同的模式，所以可以快速扫描并匹配输入的字符串。对于每个字符串，我们会返回一个 token，之后用于语法分析中。

我通过 Lex 实现 Scanner 部分。核心部分代码如下：

	digit [0-9]
	real {digit}+\.{digit}+
	number {digit}+
	letter [a-zA-Z]
	identifier {letter}+
	newline \n
	whitespace [ \t]+
	char \'.\'
	string \".*\"

	%%

	"program"       {return TOKEN_PROGRAM;}
	"if"        {return TOKEN_IF;}
	"then"      {return TOKEN_THEN;}
	"else"      {return TOKEN_ELSE;}
	"repeat"    {return TOKEN_REPEAT;}
	"until"     {return TOKEN_UNTIL;}
	"while"     {return TOKEN_WHILE;}
	"do"        {return TOKEN_DO;}
	"case"      {return TOKEN_CASE;}
	"to"        {return TOKEN_TO;}
	"downto"    {return TOKEN_DOWNTO;}
	"for"       {return TOKEN_FOR;}
	"read"      {return TOKEN_READ;}
	"write"     {return TOKEN_WRITE;}
	"writeln"   {return TOKEN_WRITELN;}

	"["         {return TOKEN_LB;}
	"]"         {return TOKEN_RB;}
	";"         {return TOKEN_SEMI;}
	".."        {return TOKEN_DOTDOT;}
	"."         {return TOKEN_DOT;}
	"("         {return TOKEN_LP;}
	")"         {return TOKEN_RP;}
	","         {return TOKEN_COMMA;}
	":"         {return TOKEN_COLON;}

	":="        {return TOKEN_ASSIGN;}
	"="         {return TOKEN_EQUAL;} 
	"+"         {return TOKEN_PLUS;}
	"-"         {return TOKEN_MINUS;}
	"or"        {return TOKEN_OR;}
	"<>"        {return TOKEN_UNEQUAL;}
	">="        {return TOKEN_GE;}
	">"         {return TOKEN_GT;}
	"<="        {return TOKEN_LE;}
	"<"         {return TOKEN_LT;}
	"*"         {return TOKEN_MUL;}
	"\\"        {return TOKEN_DIV;}
	"mod"       {return TOKEN_MOD;}
	"and"       {return TOKEN_AND;}
	"not"       {return TOKEN_NOT;}

	"goto"      {return TOKEN_GOTO;}
	"integer"   {return TOKEN_INTEGER_TYPE;}
	"boolean"   {return TOKEN_BOOLEAN_TYPE;}
	"char"      {return TOKEN_CHAR_TYPE;}
	"real"      {return TOKEN_REAL_TYPE;}
	"true"      {return TOKEN_TRUE;}
	"false"     {return TOKEN_FALSE;}
	"maxint"    {return TOKEN_MAXINT;}
	"array"     {return TOKEN_ARRAY;}
	"of"        {return TOKEN_OF;}
	"record"    {return TOKEN_RECORD;}
	"begin"     {return TOKEN_BEGIN;}
	"end"       {return TOKEN_END;}
	"const"     {return TOKEN_CONST;}
	"type"      {return TOKEN_TYPE;}
	"var"       {return TOKEN_VAR;}
	"function"  {return TOKEN_FUNCTION;}
	"procedure" {return TOKEN_PROCEDURE;}
	{number}    {return TOKEN_INT;}
	{real}      {return TOKEN_REAL;}
	{char}      {return TOKEN_CHAR;}
	{string}    {return TOKEN_STRING;}
	{identifier} {return TOKEN_ID;}
	{newline}   {lineno++;}
	{whitespace}    {}
	"{"[^{}]*"}"  {/* ignore comments */}
	%%

### 语法分析

Yacc（Yet Another Compiler Compiler），是一个用来生成编译器的编译器。yacc生成的编译器主要是用C语言写成的语法解析器，需要与词法解析器Lex一起使用，再把两部分产生出来的C程序一并编译。
yacc的输入是巴科斯范式（BNF）表达的语法规则以及语法规约的处理代码，Yacc输出的是基于表驱动的编译器，包含输入的语法规约的处理代码部分。
yacc是开发编译器的一个有用的工具，采用LALR语法分析方法。
yacc最初由AT&T的Steven C. Johnson为Unix操作系统开发，后来一些兼容的程序如Berkeley Yacc，GNU bison，MKS yacc和Abraxas yacc陆续出现。它们都在原先基础上做了少许改进或者增加，但是基本概念是相同的。

我通过 Yacc 实现 Scanner 部分。参考提供的 Pascal Syntax 和 Tiny complier 的实现。

### 代码生成

我利用通过 Yacc 生成的语法树，来生成 MASM32 的汇编代码。

主要有三个组成部分：

* 生成程序的头部
* 生成之后的代码
* 生成程序的尾部

对于程序的头部来说，首先是一些例行公事的代码，另外附加上一些可能多次用到的函数，例如输入输出的函数，还有一些常量。最后要生成进入主程序的代码。

之后就是重头戏，生成程序的代码。首先是确定当前这句话的类型：

    switch (pnode->nodekind) {
        case NODE_STATEMENT:
            GStmtCode(pnode);
            break;
        case NODE_EXPRESSION:
            GExpCode(pnode, 0);
            break;
        case NODE_DECLARE:
            GDeclCode(pnode);
            break;
        default:
            break;
    }

首先对于 Statement 来说：

    switch (pnode->kind.stmt) {
        case STMT_IF:
            GStmtIfCode(pnode);
            break;
        case STMT_WHILE:
            GStmtWhileCode(pnode);
            break;
        case STMT_REPEAT:
            GStmtRepeatCode(pnode);
            break;
        case STMT_FOR:
            GStmtForCode(pnode);
            break;
        case STMT_PROC_SYS: {
            switch (pnode->attr.op) {
                case TOKEN_READ:
                    GStmtInputCode(pnode);
                    break;
                case TOKEN_WRITE:
                    // TODO: remove new line
                    GStmtOutputCode(pnode);
                    break;
                case TOKEN_WRITELN:
                    // TODO: fix new line
                    GStmtOutputCode(pnode);
                    // GStmtNewLine(pnode);
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }

对于 Expression 来说：

    switch (pnode->kind.exp) {
        case EXP_FUNC:
            CG_OUTPUT(";call function\n");
            GExpCallCode(pnode);
            break;
        case EXP_OP:
            GExpOpCode(pnode);
            break;
        case EXP_CONST:
            GExpConstCode(pnode);
            break;
        case EXP_ID:
            //if the ID is a function ,first execute this function
            if ((st_execute(pnode, pnode->lineno).type == EXPTYPE_FUNC) && (pnode->child[1] == NULL) &&
                (isrightvalue == 1)) {  //是函数变量
                printf("%d\n", pnode->child[1] == NULL);
                sprintf(tmp, ";the ID is function\ncall %s\n", pnode->attr.name);
                CG_OUTPUT(tmp);
            }
            else {
                sprintf(tmp, "mov edi, %d\n", getOffset(pnode));
                if (isFatherParameter(pnode)) {
                    strcat(tmp, "mov eax, ss:[esi+edi] \n");
                }
                else {
                    strcat(tmp, "mov eax, ss:[ebp+edi] \n");//st_execute(pnode, pnode->lineno).offset);
                }
                CG_OUTPUT(tmp);
            }
            break;
    }

对于 Declare 来说：

    switch (pnode->kind.decl) {
        case DECL_ROUTINEHEAD:
            printf("DECL_ROUTINEHEAD: %s\n", pnode->attr.name);
            if (pnode->child[2] != NULL) {
                st_insertdecl_var_list(pnode->child[2]);//only insert the var into symtable
            }
            if (pnode->child[3] != NULL) {
                generateCode(pnode->child[3]); //generate the function(procedure) code

            }
            if (pnode->attr.name != NULL) {
                sprintf(tmp, "%s:\n", pnode->attr.name);
                CG_OUTPUT(tmp);
                CG_OUTPUT("push ebp\nmov ebp, esp\nmov esi, ss:[ebp+0]\n");
            }
            for (i = 0; i < 2; i++) {
                if (pnode->child[i] != NULL) {
                    generateCode(pnode->child[i]);
                }
            }
            if (pnode->child[2] != NULL) {
                GDeclCode(pnode->child[2]); // use this function can void insert the var two times
            }

            break;
        case DECL_FUNCTION:
            return GDeclFuncCode(pnode);
        case DECL_PROCEDURE:
            return GDeclFuncCode(pnode);
        case DECL_VAR:
            pnode = pnode->child[0];
            while (pnode != NULL) {
                GDeclVarCode(pnode);
                pnode = pnode->sibling;
            }
            break;
        case DECL_VAL_PARA:
        case DECL_VAR_PARA:
            GParamCode(pnode);
            break;
        default:
            printf("Error %d(%d): Unkown Decl Type. Please Check\n", ++err_cnt, pnode->lineno);
            break;
    }

最后是生成结束程序的代码，主要工作是把该 pop 出来的 pop 出来，维护栈的完整。


## 实验测试

### 如何运行

	./pascal example.pas example.asm
	
之后将在当前目录下生成 out.asm，为 MASM32 位汇编语言。可以到 http://masm32.com/ 下载运行环境。

### 基本语句

样例 Pascal 代码如下:

	program basic;
	var x, y, z : integer;
	begin
	  x := 2;
	  writeln(x);
	  y := 7;
	  writeln(y);
	  z := 9;
	  writeln(z);
	  x := y + z;
	  writeln(x);
	  x := y - z;
	  writeln(x);
	  x := y * z;
	  writeln(x);
	  x := y / z;
	  writeln(x);
	end.

分析得到如下信息：

	1: reserved word: program
	1: ID name=bas
	1: ;
	2: reserved word: var
	2: ID name=x
	2: ,
	2: ID name=y
	2: ,
	2: ID name=z
	2: :
	2: TYPE, name=integer
	2: ;
	3: reserved word: begin
	4: ID name=x
	4: Unknown token::=
	4: INT, val=2
	4: ;
	5: writeln
	5: (
	5: ID name=x
	5: )
	5: ;
	6: ID name=y
	6: Unknown token::=
	6: INT, val=7
	6: ;
	7: writeln
	7: (
	7: ID name=y
	7: )
	7: ;
	8: ID name=z
	8: Unknown token::=
	8: INT, val=9
	8: ;
	9: writeln
	9: (
	9: ID name=z
	9: )
	9: ;
	10: ID name=x
	10: Unknown token::=
	10: ID name=y
	10: +
	10: ID name=z
	10: ;
	11: writeln
	11: (
	11: ID name=x
	11: )
	11: ;
	12: ID name=x
	12: Unknown token::=
	12: ID name=y
	12: -
	12: ID name=z
	12: ;
	13: writeln
	13: (
	13: ID name=x
	13: )
	13: ;
	14: ID name=x
	14: Unknown token::=
	14: ID name=y
	14: *
	14: ID name=z
	14: ;
	15: writeln
	15: (
	15: ID name=x
	15: )
	15: ;
	16: reserved word: end
	16: .
	17: EOF

语法树部分如下：

    Routine Head
        Var Declare:
            Exp ID: x
            Exp ID: y
            Exp ID: z
            type integer
    EXP op: Unknown token:
        Exp ID: x
        const int:2
    system procedure:writeln
        Exp ID: x
    EXP op: Unknown token:
        Exp ID: y
        const int:7
    system procedure:writeln
        Exp ID: y
    EXP op: Unknown token:
        Exp ID: z
        const int:9
    system procedure:writeln
        Exp ID: z
    EXP op: Unknown token:
        Exp ID: x
        EXP op: +
            Exp ID: y
            Exp ID: z
    system procedure:writeln
        Exp ID: x
    EXP op: Unknown token:
        Exp ID: x
        EXP op: -
            Exp ID: y
            Exp ID: z
    system procedure:writeln
        Exp ID: x
    EXP op: Unknown token:
        Exp ID: x
        EXP op: *
            Exp ID: y
            Exp ID: z
    system procedure:writeln
        Exp ID: x

生成的 MASM32 代码:

	.386
	.model flat,stdcall
	option casemap:none

	include    masm32\include\windows.inc
	include    masm32\include\kernel32.inc
	include    masm32\include\msvcrt.inc
	includelib masm32\lib\msvcrt.lib
	includelib masm32\lib\kernel32.lib
	include    masm32\include\user32.inc
	includelib masm32\lib\user32.lib
	includelib masm32\lib\kernel32.lib
	includelib masm32\lib\msvcrt.lib
	printf PROTO C :ptr sbyte, :vararg
		.data
	szInput        db  '%d', 0
	szFormat    db    '%d',0ah, 0dh , 0
	tmp			db 0, 0, 0, 0, 0, 0, 0, 0
	    .const
	    .code 
	start:  
	mov esi, ebp
		push ebp
		mov ebp, esp
		call basic
	pop ebp

	    invoke    ExitProcess,NULL

	output:
		push    ebp
	    mov     ebp, esp
	    push    eax
	    mov     ebx, offset szFormat
	    push    ebx
	    call    printf

		pop		ebx
		pop		ebx

	    pop     ebp
	    ret
		ret
	input:
		invoke crt_scanf, addr szInput, addr tmp
		mov eax, dword ptr tmp
		ret
	basic:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]
	push eax		; Allocate the space in stack to a Varable
	push eax		; Allocate the space in stack to a Varable
	push eax		; Allocate the space in stack to a Varable

	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 2
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 7
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -8
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -12
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 9
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -12
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -12
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -8
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -12
	mov eax, ss:[ebp+edi] 
	mov ebx, eax
	pop eax
	add eax, ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -8
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -12
	mov eax, ss:[ebp+edi] 
	mov ebx, eax
	pop eax
	sub eax, ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -8
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -12
	mov eax, ss:[ebp+edi] 
	mov ebx, eax
	pop eax
	xor edx, edx
	mul ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	add esp, 12
	pop ebp
	ret
	end    start

运行结果如下：

![Basic](images/basic.png)

### 循环

支持 for、while、repeat。

测试程序如下：

	program right;
	var a: integer;
	begin

	a := 1;

	repeat
	begin
	    writeln(a);
	    a := a + 1;
	end;
	until a = 3;

	for a := 1 to 3 do
	begin
	    writeln(99);
	end;

	while a < 6 do
	begin
	  writeln (a);
	  a := a + 1;
	end;

	end.


词法分析和语法分析：

	1: reserved word: program
	1: ID name=right
	1: ;
	2: reserved word: var
	2: ID name=a
	2: :
	2: TYPE, name=integer
	2: ;
	3: reserved word: begin
	5: ID name=a
	5: Unknown token::=
	5: INT, val=1
	5: ;
	7: Unknown token:repeat
	8: reserved word: begin
	9: writeln
	9: (
	9: ID name=a
	9: )
	9: ;
	10: ID name=a
	10: Unknown token::=
	10: ID name=a
	10: +
	10: INT, val=1
	10: ;
	11: reserved word: end
	11: ;
	12: Unknown token:until
	12: ID name=a
	12: =
	12: INT, val=3
	12: ;
	14: Unknown token:for
	14: ID name=a
	14: Unknown token::=
	14: INT, val=1
	14: to
	14: INT, val=3
	14: Unknown token:do
	15: reserved word: begin
	16: writeln
	16: (
	16: INT, val=99
	16: )
	16: ;
	17: reserved word: end
	17: ;
	19: Unknown token:while
	19: ID name=a
	19: <
	19: INT, val=6
	19: Unknown token:do
	20: reserved word: begin
	21: writeln
	21: (
	21: ID name=a
	21: )
	21: ;
	22: ID name=a
	22: Unknown token::=
	22: ID name=a
	22: +
	22: INT, val=1
	22: ;
	23: reserved word: end
	23: ;
	25: reserved word: end
	25: .
	26: EOF
    Routine Head
        Var Declare:
            Exp ID: a
            type integer
    EXP op: Unknown token:
        Exp ID: a
        const int:1
    repeat
        system procedure:writeln
            Exp ID: a
        EXP op: Unknown token:
            Exp ID: a
            EXP op: +
                Exp ID: a
                const int:1
        EXP op: =
            Exp ID: a
            const int:3
    for direction:to
        Exp ID: a
        const int:1
        const int:3
        system procedure:writeln
            const int:99
    while
        EXP op: <
            Exp ID: a
            const int:6
        system procedure:writeln
            Exp ID: a
        EXP op: Unknown token:
            Exp ID: a
            EXP op: +
                Exp ID: a
                const int:1

汇编代码如下：

	.386
	.model flat,stdcall
	option casemap:none

	include    masm32\include\windows.inc
	include    masm32\include\kernel32.inc
	include    masm32\include\msvcrt.inc
	includelib masm32\lib\msvcrt.lib
	includelib masm32\lib\kernel32.lib
	include    masm32\include\user32.inc
	includelib masm32\lib\user32.lib
	includelib masm32\lib\kernel32.lib
	includelib masm32\lib\msvcrt.lib
	printf PROTO C :ptr sbyte, :vararg
		.data
	szInput        db  '%d', 0
	szFormat    db    '%d',0ah, 0dh , 0
	tmp			db 0, 0, 0, 0, 0, 0, 0, 0
	    .const
	    .code 
	start:  
	mov esi, ebp
		push ebp
		mov ebp, esp
		call right
	pop ebp

	    invoke    ExitProcess,NULL

	output:
		push    ebp
	    mov     ebp, esp
	    push    eax
	    mov     ebx, offset szFormat
	    push    ebx
	    call    printf

		pop		ebx
		pop		ebx

	    pop     ebp
	    ret
		ret
	input:
		invoke crt_scanf, addr szInput, addr tmp
		mov eax, dword ptr tmp
		ret
	right:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]
	push eax		; Allocate the space in stack to a Varable

	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	__CG__label0:		;repeat start
	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	add eax, ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 3
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	sete al

	cmp eax, 0
	 je __CG__label0		;repeat continue
	__CG__label1:


	mov eax, 1

	mov ecx, eax 		;ecx = for1
	mov eax, 3

	mov edx, eax 		;edx = for2
	inc edx
	__CG__label2:
	 cmp ecx, edx
	 je __CG__label3		;for end
	push edx		;save for2
	push ecx		;save for1
	;function parameter start
	mov eax, 99
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	pop ecx		;restore for1
	pop edx		;restore for2
	inc ecx
	 jmp __CG__label2
	 __CG__label3:


	__CG__label4:		;while start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 6
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	setl al

	cmp eax, 0
	 je __CG__label5		;while break
	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	add eax, ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	jmp __CG__label4
	__CG__label5:


	add esp, 4
	pop ebp
	ret
	end    start

	
运行结果如下：

![Basic](images/loop.png)


### 函数

以老师提供的 test2.asm 为例。

测试程序如下：

	program hello;
	var
	    i : integer;

	function go(a : integer): integer;
	begin
	    if a = 1 then
	    begin
	        go := 100;
	    end
	    else
	    begin
	        if a = 2 then
	        begin
	            go := 90;
	        end
	        else
	        begin
	            go := 80;
	        end;
	    end;
	end;

	begin
	    i := go(1);
	    write(i);
	    i := go(2);
	    write(i);
	    i := go(3);
	    write(i);
	end.

词法分析以及语法分析如下：

	1: reserved word: program
	1: ID name=hello
	1: ;
	2: reserved word: var
	3: ID name=i
	3: :
	3: TYPE, name=integer
	3: ;
	5: reserved word: function
	5: ID name=go
	5: (
	5: ID name=a
	5: :
	5: TYPE, name=integer
	5: )
	5: :
	5: TYPE, name=integer
	5: ;
	6: reserved word: begin
	7: reserved word: if
	7: ID name=a
	7: =
	7: INT, val=1
	7: reserved word: then
	8: reserved word: begin
	9: ID name=go
	9: Unknown token::=
	9: INT, val=100
	9: ;
	10: reserved word: end
	11: reserved word: else
	12: reserved word: begin
	13: reserved word: if
	13: ID name=a
	13: =
	13: INT, val=2
	13: reserved word: then
	14: reserved word: begin
	15: ID name=go
	15: Unknown token::=
	15: INT, val=90
	15: ;
	16: reserved word: end
	17: reserved word: else
	18: reserved word: begin
	19: ID name=go
	19: Unknown token::=
	19: INT, val=80
	19: ;
	20: reserved word: end
	20: ;
	21: reserved word: end
	21: ;
	22: reserved word: end
	22: ;
	functional:go	24: reserved word: begin
	25: ID name=i
	25: Unknown token::=
	25: ID name=go
	25: (
	25: INT, val=1
	25: )
	25: ;
	26: write
	26: (
	26: ID name=i
	26: )
	26: ;
	27: ID name=i
	27: Unknown token::=
	27: ID name=go
	27: (
	27: INT, val=2
	27: )
	27: ;
	28: write
	28: (
	28: ID name=i
	28: )
	28: ;
	29: ID name=i
	29: Unknown token::=
	29: ID name=go
	29: (
	29: INT, val=3
	29: )
	29: ;
	30: write
	30: (
	30: ID name=i
	30: )
	30: ;
	31: reserved word: end
	31: .
	32: EOF
    Routine Head
        Var Declare:
            Exp ID: i
            type integer
        Declare Function 
            type integer
            val parameters:
                Exp ID: a
                type integer
            Routine Head
            if
                EXP op: =
                    Exp ID: a
                    const int:1
                EXP op: Unknown token:
                    Exp ID: go
                    const int:100
                if
                    EXP op: =
                        Exp ID: a
                        const int:2
                    EXP op: Unknown token:
                        Exp ID: go
                        const int:90
                    EXP op: Unknown token:
                        Exp ID: go
                        const int:80
    EXP op: Unknown token:
        Exp ID: i
        Unknown Expression type
            const int:1
    system procedure:write
        Exp ID: i
    EXP op: Unknown token:
        Exp ID: i
        Unknown Expression type
            const int:2
    system procedure:write
        Exp ID: i
    EXP op: Unknown token:
        Exp ID: i
        Unknown Expression type
            const int:3
    system procedure:write
        Exp ID: i


汇编代码如下：

	.386
	.model flat,stdcall
	option casemap:none

	include    masm32\include\windows.inc
	include    masm32\include\kernel32.inc
	include    masm32\include\msvcrt.inc
	includelib masm32\lib\msvcrt.lib
	includelib masm32\lib\kernel32.lib
	include    masm32\include\user32.inc
	includelib masm32\lib\user32.lib
	includelib masm32\lib\kernel32.lib
	includelib masm32\lib\msvcrt.lib
	printf PROTO C :ptr sbyte, :vararg
		.data
	szInput        db  '%d', 0
	szFormat    db    '%d',0ah, 0dh , 0
	tmp			db 0, 0, 0, 0, 0, 0, 0, 0
	    .const
	    .code 
	start:  
	mov esi, ebp
		push ebp
		mov ebp, esp
		call hello
	pop ebp

	    invoke    ExitProcess,NULL

	output:
		push    ebp
	    mov     ebp, esp
	    push    eax
	    mov     ebx, offset szFormat
	    push    ebx
	    call    printf

		pop		ebx
		pop		ebx

	    pop     ebp
	    ret
		ret
	input:
		invoke crt_scanf, addr szInput, addr tmp
		mov eax, dword ptr tmp
		ret


	go:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]

	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	sete al

	cmp eax, 1
	je __CG__label0		; If equal Jump to taken-content; 
	jmp __CG__label1		; Jump to NotTake Content
	__CG__label0: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov eax, 100
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	jmp __CG__label2		; Jump to The if exit Label
	__CG__label1: 
	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 2
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	sete al

	cmp eax, 1
	je __CG__label3		; If equal Jump to taken-content; 
	jmp __CG__label4		; Jump to NotTake Content
	__CG__label3: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov eax, 90
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	jmp __CG__label5		; Jump to The if exit Label
	__CG__label4: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov eax, 80
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	__CG__label5: 


	__CG__label2: 


	add esp, 0
	pop ebp
	ret

	hello:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]
	push eax		; Allocate the space in stack to a Varable

	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;call function
	;function parameter start
	mov eax, 1
	push eax
	;function parameter end
	call go
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;call function
	;function parameter start
	mov eax, 2
	push eax
	;function parameter end
	call go
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;call function
	;function parameter start
	mov eax, 3
	push eax
	;function parameter end
	call go
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	add esp, 4
	pop ebp
	ret
	end    start


运行结果如下：

![Basic](images/func.png)

### 递归

以求斐波纳挈数列第 N 项为例。

测试程序如下：

	program hello;
	var 
	    ans : integer;

	function func(a : integer) : integer;
	begin
	    if a = 1 then begin
	        func := 1;
	    end
	    else begin
	        if a = 2 then begin
	            func := 1;
	        end
	        else begin
	            func := func(a - 2);
	            func := func + func(a - 1);
	        end;
	    end;
	end;

	begin
	    ans := func(5);
	    writeln(ans);
	end.

生成的汇编代码如下：

	.386
	.model flat,stdcall
	option casemap:none

	include    masm32\include\windows.inc
	include    masm32\include\kernel32.inc
	include    masm32\include\msvcrt.inc
	includelib masm32\lib\msvcrt.lib
	includelib masm32\lib\kernel32.lib
	include    masm32\include\user32.inc
	includelib masm32\lib\user32.lib
	includelib masm32\lib\kernel32.lib
	includelib masm32\lib\msvcrt.lib
	printf PROTO C :ptr sbyte, :vararg
		.data
	szInput        db  '%d', 0
	szFormat    db    '%d',0ah, 0dh , 0
	tmp			db 0, 0, 0, 0, 0, 0, 0, 0
	    .const
	    .code 
	start:  
	mov esi, ebp
		push ebp
		mov ebp, esp
		call hello
	pop ebp

	    invoke    ExitProcess,NULL

	output:
		push    ebp
	    mov     ebp, esp
	    push    eax
	    mov     ebx, offset szFormat
	    push    ebx
	    call    printf

		pop		ebx
		pop		ebx

	    pop     ebp
	    ret
		ret
	input:
		invoke crt_scanf, addr szInput, addr tmp
		mov eax, dword ptr tmp
		ret


	func:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]

	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	sete al

	cmp eax, 1
	je __CG__label0		; If equal Jump to taken-content; 
	jmp __CG__label1		; Jump to NotTake Content
	__CG__label0: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	jmp __CG__label2		; Jump to The if exit Label
	__CG__label1: 
	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 2
	mov ebx, eax
	pop eax
	cmp eax, ebx
	mov eax, 0
	sete al

	cmp eax, 1
	je __CG__label3		; If equal Jump to taken-content; 
	jmp __CG__label4		; Jump to NotTake Content
	__CG__label3: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	jmp __CG__label5		; Jump to The if exit Label
	__CG__label4: 
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	;call function
	;function parameter start
	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 2
	mov ebx, eax
	pop eax
	sub eax, ebx
	push eax
	;function parameter end
	call func
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	mov edi, -8
	mov eax, ss:[esi+edi] 
	push eax
	;call function
	;function parameter start
	mov edi, 8
	mov eax, ss:[ebp+edi] 
	push eax
	mov eax, 1
	mov ebx, eax
	pop eax
	sub eax, ebx
	push eax
	;function parameter end
	call func
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	add eax, ebx
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -8
	mov ss:[esi+edi], eax

	__CG__label5: 


	__CG__label2: 


	add esp, 0
	pop ebp
	ret

	hello:
	push ebp
	mov ebp, esp
	mov esi, ss:[ebp+0]
	push eax		; Allocate the space in stack to a Varable

	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;call function
	;function parameter start
	mov eax, 5
	push eax
	;function parameter end
	call func
	pop ebx		;pop parameter in order to banlance the stack
	mov ebx, eax
	pop eax
	mov eax, ebx
	mov edi, -4
	mov ss:[ebp+edi], eax

	;function parameter start
	mov edi, -4
	mov eax, ss:[ebp+edi] 
	push eax
	;function parameter end
	call output
	pop ebx		;pop parameter in order to banlance the stack


	add esp, 4
	pop ebp
	ret
	end    start

运行程序截图：

![Basic](images/fib.png)


## 总结

我认识中加班的同学，他曾经和我说，在 SFU，他们的编译器都是每个人单独完成的。
他们最后的项目是用 Java 写一个简化的 Java 编译器。
我觉得我也可以。

但奈何毕业事情太多，没有做得尽善尽美，希望毕业之后可以有机会继续提升吧。
回想起来，本科几年对待课程的态度都不够认真。
希望之后在研究生阶段可以更尽善尽美一些。